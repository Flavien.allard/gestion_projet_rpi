import RPi.GPIO as GPIO
import time

# DEFINE PINS
led_red = 12
led_blue = 16
led_green = 20
led_rgb_red = 17
led_rgb_blue = 27
led_rgb_green = 22

# SETTING PIN MODES
GPIO.setmode(GPIO.BCM)

# SETTING UP INPUTS AND OUTPUTS
GPIO.setup(led_red, GPIO.OUT)
GPIO.setup(led_blue, GPIO.OUT)
GPIO.setup(led_green, GPIO.OUT)
GPIO.setup(led_rgb_red, GPIO.OUT)
GPIO.setup(led_rgb_green, GPIO.OUT)
GPIO.setup(led_rgb_blue, GPIO.OUT)

def switch_led_on(pin_number):
    GPIO.output(pin_number,1)

def switch_led_off(pin_number):
    GPIO.output(pin_number,0)








# SWITCHING EVERYTHING OFF
"""
GPIO.output(led_red,1)
GPIO.output(led_blue,1)
GPIO.output(led_green,1)
GPIO.output(led_rgb_red,1)
GPIO.output(led_rgb_blue,1)
GPIO.output(led_rgb_green,1)

time.sleep(5)

GPIO.output(led_red,0)
GPIO.output(led_rgb_red,0)

time.sleep(5)

GPIO.output(led_blue,0)
GPIO.output(led_rgb_blue,0)

time.sleep(5)

GPIO.output(led_green,0)
GPIO.output(led_rgb_green,0)
"""
