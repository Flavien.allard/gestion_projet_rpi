import sys
import RPi.GPIO as GPIO
import time
import threading

GPIO.setmode(GPIO.BCM)

BUZZER = 21

GPIO.setup(BUZZER,GPIO.OUT)

buzz_duration = 0
buzz_down = 1 - buzz_duration

def buzz(noteFreq, duration):
    halveWaveTime = 1 / (noteFreq * 2 )
    waves = int(duration * noteFreq)
    for i in range(waves):
       GPIO.output(BUZZER, True)
       time.sleep(halveWaveTime)
       GPIO.output(BUZZER, False)
       time.sleep(halveWaveTime)

def buzzer_music():
    t=0
    notes=[262,294,330,262,262,294,330,262,330,349,392,330,349,392,392,440,392,349,330,262,392,440,392,349,330,262,262,196,262,262,196,262]
    duration=[0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1,0.5,0.5,1,0.25,0.25,0.25,0.25,0.5,0.5,0.25,0.25,0.25,0.25,0.5,0.5,0.5,0.5,1,0.5,0.5,1]
    for n in notes:
        buzz(n, duration[t])
        time.sleep(duration[t] *0.1)
        t+=1

def set_buzzer_duration(value):
    global buzz_down
    global buzz_duration
    buzz_down = 1 - value
    buzz_duration = value

def bipping_buzzer():
    while True:
        buzz(440,buzz_duration)
        time.sleep(buzz_down)


threadBuzzer = threading.Thread(None, bipping_buzzer, None, (), {})
threadBuzzer.start()
