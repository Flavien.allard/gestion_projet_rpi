from connection_to_server import *
from server import *
from distance import *
from temp import *
#from arduino_communication import *
import time

while True:
    try:
        distance = get_distance()
        sendServerMessage(f"09:{distance}")
        temperature = get_temp()
        sendServerMessage(f"07:{temperature}")
        humidity = get_humidity()
        sendServerMessage(f"08:{humidity}")
        time.sleep(2)
    except KeyboardInterrupt:
        GPIO.cleanup()

# sendServerMessage("03:245")

while True:
    print("alive")
    time.sleep(5)
