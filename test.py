import RPi.GPIO as GPIO
import time


#GPIO.setmode(GPIO.BCM)

# DEFINE PINS
led_red = 32
led_blue = 36
led_green = 38

# SETTING PIN MODES
GPIO.setmode(GPIO.BOARD)


# SETTING UP INPUTS AND OUTPUTS
GPIO.setup(led_red, GPIO.OUT)
GPIO.setup(led_blue, GPIO.OUT)
GPIO.setup(led_green, GPIO.OUT)

# SWITCHING EVERYTHING OFF
GPIO.output(led_red,1)
GPIO.output(led_blue,1)
GPIO.output(led_green,1)

time.sleep(5)

GPIO.output(led_red,0)

time.sleep(5)

GPIO.output(led_blue,0)

time.sleep(5)

GPIO.output(led_green,0)

GPIO.cleanup()
