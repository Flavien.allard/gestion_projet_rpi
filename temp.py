import time
import board
import adafruit_dht
import psutil

# We first check if a libgpiod process is running. If yes, we kill it!
for proc in psutil.process_iter():
    if proc.name() == 'libgpiod_pulsein' or proc.name() == 'libgpiod_pulsei':
        proc.kill()

sensor_temp = adafruit_dht.DHT11(board.D23)

def get_humidity():
    try:
        humidity = sensor_temp.humidity
        return humidity
    except RuntimeError:
        return get_humidity()

def get_temp():
    try:
        temp = sensor_temp.temperature
        return temp
    except RuntimeError:
        return get_temp()
