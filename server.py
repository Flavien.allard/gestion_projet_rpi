from leds import *
from rgb_leds import *
from buzzer import *

import threading
import socket

# Function for interpreting messages and acting on circuit
def process_message(message):
    
    message_content = message.split(":")

    try:
        float(message_content[0])
        float(message_content[1])
    except ValueError:
        print("Invalid value")

    # RED LED
    if int(message_content[0]) == 1:
        if (int(message_content[1]) == 0):
            switch_led_off(led_red)
        else:
            switch_led_on(led_red)
    # GREEN LED
    if int(message_content[0]) == 2:
        if (int(message_content[1]) == 0):
            switch_led_off(led_green)
        else:
            switch_led_on(led_green)
    # BLUE LED
    if int(message_content[0]) == 3:
        if (int(message_content[1]) == 0):
            switch_led_off(led_blue)
        else:
            switch_led_on(led_blue)
    # RGB RED LED
    if int(message_content[0]) == 4:
        if int(message_content[1]) >= 0 and int(message_content[1]) <= 100:
            set_rgb_red(int(message_content[1]))

    # RGB GREEN LED
    if int(message_content[0]) == 5:
        if int(message_content[1]) >= 0 and int(message_content[1]) <= 100:
            set_rgb_green(int(message_content[1]))

    # RGB BLUE LED
    if int(message_content[0]) == 6:
        if int(message_content[1]) >= 0 and int(message_content[1]) <= 100:
            set_rgb_blue(int(message_content[1]))

    if int(message_content[0]) == 10:
        if float(message_content[1]) >= 0 and float(message_content[1]) <= 1:
            set_buzzer_duration(float(message_content[1]))

# Starting listening server
def start_server():
    print("Starting server...")
    rpi_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    rpi_server.bind(("", 5575))
    rpi_server.listen(1)
    client, infosClient = rpi_server.accept()
    print("New incoming connection...")
    flag = True
    while flag:
        message = client.recv(255)
        message = message.decode("utf-8")
        # ARRET
        if message == "fin":
            flag = False
            client.close()
            break
        process_message(message)

# Creating thread for server
threadServeur = threading.Thread(None, start_server, None, (), {})
threadServeur.start()


