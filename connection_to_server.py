import socket

serverIP = "10.11.1.201"	# Ici, le poste local
serverPort = 5575	# Se connecter sur le port 50000

print(f"Trying to connect to {serverIP}:{serverPort} ...")

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.connect((serverIP, serverPort))

print("Connected to server !")

def sendServerMessage(message):
    serverSocket.send(message.encode("utf_8"))

def closeSocket():
    serverSocket.close()
