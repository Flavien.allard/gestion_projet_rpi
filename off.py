import RPi.GPIO as GPIO
import time

# SETTING PIN MODES
GPIO.setmode(GPIO.BOARD)

for i in range(1,40):
    if i not in (1,2,4,6,9,14,17,20,25,27,28,30,34,39):
        print(i)
        GPIO.setup(i,GPIO.OUT)
        GPIO.output(i,0)


